<?php namespace Gogo\Blog\Tests;

use PluginTestCase;
use Gogo\Blog\Models\Tag;

class TagTest extends PluginTestCase
{
	public function setup(){
		parent::setup();

		include plugins_path('gogo/blog/routes.php');
	}

	public function testTagList()
	{
		$response = $this->json('GET', '/v1/blog/tags');
		$response->assertStatus(200);
		$response->assertJsonStructure(['total', 'data', 'per_page']);
	}
}
