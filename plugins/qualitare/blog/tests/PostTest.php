<?php namespace Gogo\Blog\Tests;

use PluginTestCase;
use Gogo\Blog\Models\Post;

class PostTest extends PluginTestCase
{
	public function setup(){
		parent::setup();

		include plugins_path('gogo/blog/routes.php');
	}

	public function testPostList()
	{
		// Success case
		$response = $this->json('GET', '/v1/blog/posts');
		$response->assertStatus(200);
		$response->assertJsonStructure(['total', 'data', 'per_page']);
	}

	public function testPostFeatured()
	{
		// Success case
		$response = $this->json('GET', '/v1/blog/posts/featured');
		$response->assertStatus(200);
		$response->assertJsonStructure(['total', 'data', 'per_page']);
	}

	public function testPostCategoryFilter()
	{
                $post = Post::whereHas('category')->first();
                
		// Not found case
		$response = $this->json('GET', '/v1/blog/posts/category/this-cannot-continue');
		$response->assertStatus(404);

		// Success case
		$response = $this->json('GET', '/v1/blog/posts/category/' . $post->category->slug);
		$response->assertStatus(200);
		$response->assertJsonStructure(['total', 'data', 'per_page']);
	}

	public function testPostSearch()
	{
		$post = Post::first();

		// Not found case
		$response = $this->json('GET', '/v1/blog/posts/search/o-rato-roeu-a-roupa-do-rei-de-roma');
		$response->assertStatus(404);

		// Success case
		$response = $this->json('GET', '/v1/blog/posts/search/' . $post->title);
		$response->assertStatus(200);
		$response->assertJsonStructure(['total', 'data', 'per_page']);
	}

	public function testPostShow()
	{
		$post = Post::first();

		// Not found case
		$response = $this->json('GET', '/v1/blog/posts/123456');
		$response->assertStatus(404);

		// Success case
		$response = $this->json('GET', '/v1/blog/posts/' . $post->slug);
		$response->assertStatus(200);
	}
}
