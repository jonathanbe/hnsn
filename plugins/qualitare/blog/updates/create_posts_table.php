<?php namespace Qualitare\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
	public function up()
	{
		Schema::create('qualitare_blog_posts', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('title');
			$table->boolean('featured');
			$table->string('slug');
			$table->string('excerpt');
			$table->text('content')->nullable();
			$table->enum('status', ['published', 'review'])->default('review');
			$table->string('category_id');
			$table->integer('views')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('qualitare_blog_posts');
	}
}
