<?php namespace Qualitare\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
	public function up()
	{
		Schema::create('qualitare_blog_categories', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name');
			$table->string('slug');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('qualitare_blog_categories');
	}
}
