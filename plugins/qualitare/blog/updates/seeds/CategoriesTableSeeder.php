<?php namespace Qualitare\Blog\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;
use Qualitare\Blog\Models\Category;
use Faker;

class CategoriesTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();

		Category::create([
			'name' => 'João Pessoa',
			'slug' => 'joao-pessoa'
		]);

		Category::create([
			'name' => 'All Urban',
			'slug' => 'all-urban'
		]);

		Category::create([
			'name' => 'Eventos',
			'slug' => 'events'
		]);
	}
}
