<?php namespace Qualitare\Blog\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;
use Qualitare\Blog\Models\Post;
use Qualitare\Blog\Models\Category;
use Illuminate\Http\UploadedFile;
use Faker;

class PostsTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();
		$featured = Category::where('slug', 'featured')->first();

		for ($i = 1; $i <= 3; $i++) {
			for ($k = 0; $k < 60; $k++) {
				$post = Post::create([
					'title' => $faker->sentence(3, true),
					'slug' => $faker->slug(3),
					'content' => $faker->text,
					'featured' => $faker->boolean(),
					'excerpt' => $faker->sentence(3),
					'category_id' => $i
				]);

				$post->thumbnail = UploadedFile::fake()->image('thumbnail.jpg');
				$post->save();
			}
		}
	}
}
