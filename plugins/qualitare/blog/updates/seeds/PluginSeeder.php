<?php

namespace Qualitare\Blog\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;

class PluginSeeder extends Seeder {

	public function run() {
		$this->call([
			CategoriesTableSeeder::class
		]);

		if (env('APP_ENV') != 'testing' && env('APP_ENV') != 'development')
			return;

		$this->call([
			PostsTableSeeder::class
		]);
	}

}
