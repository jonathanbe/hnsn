<?php namespace Qualitare\Blog;

use Backend;
use System\Classes\PluginBase;

/**
 * Blog Plugin Information File
 */
class Plugin extends PluginBase
{

	/**
	 * Register method, called when the plugin is first registered.
	 *
	 * @return void
	 */
	public function register()
	{

	}

	/**
	 * Boot method, called right before the request route.
	 *
	 * @return array
	 */
	public function boot()
	{

	}

	/**
	 * Registers any front-end components implemented in this plugin.
	 *
	 * @return array
	 */
	public function registerComponents()
	{
		return []; // Remove this line to activate

		return [
			'Qualitare\Blog\Components\MyComponent' => 'myComponent',
		];
	}

	/**
	 * Registers any back-end permissions used by this plugin.
	 *
	 * @return array
	 */
	public function registerPermissions()
	{
		return []; // Remove this line to activate

		return [
			'gogo.blog.some_permission' => [
				'tab' => 'Qualitare',
				'label' => 'Some permission'
			],
		];
	}
}
