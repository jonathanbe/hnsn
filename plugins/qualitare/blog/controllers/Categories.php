<?php namespace Qualitare\Blog\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Qualitare\Blog\Models\Category;
use Illuminate\Http\Request;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
	public $implement = [
		'Backend.Behaviors.FormController',
		'Backend.Behaviors.ListController'
	];

	public $formConfig = 'config_form.yaml';
	public $listConfig = 'config_list.yaml';

	public function __construct()
	{
		parent::__construct();

		BackendMenu::setContext('Qualitare.Blog', 'menu-blog');
	}
        
        public function list(Request $request)
	{
		$posts = Category::paginate();
		// No post was found
		if ($posts->total() == 0)
			return response(['errors' => ['Resource not found']], 404);

		return response($posts, 200);
	}
}
