<?php namespace Qualitare\Hnsn\Models;

use Model;

/**
 * Model
 */
class Newsletter extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $fillable = ['name', 'email'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_hnsn_newsletter';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
