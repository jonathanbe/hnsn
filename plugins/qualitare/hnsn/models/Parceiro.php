<?php namespace Qualitare\Hnsn\Models;

use Model;

/**
 * Model
 */
class Parceiro extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $with = ['foto'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_hnsn_parceiros';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'foto' => 'System\Models\File'
    ];
}
