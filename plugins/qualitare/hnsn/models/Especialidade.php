<?php namespace Qualitare\Hnsn\Models;

use Model;

/**
 * Model
 */
class Especialidade extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_hnsn_especialidades';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'medicos' => [
            'Qualitare\Hnsn\Models\Medicos',
            'table' => 'qualitare_hnsn_especialidade_medico'
        ],
    ];
}
