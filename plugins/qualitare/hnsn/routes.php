<?php

Route::group(['prefix' => 'v1'], function () {

        //Newsletter
        Route::group(['prefix' => 'newsletter'], function () {
			Route::post('/create', ['uses' => 'Qualitare\Hnsn\Controllers\Newsletters@insert']);
 	    });

        //Especialidades
        Route::group(['gprefix' => 'especialidades'], function () {
			Route::get('/', ['uses' => 'Qualitare\Hnsn\Controllers\Especialidades@list']);
			Route::post('/find', ['uses' => 'Qualitare\Hnsn\Controllers\Especialidades@find']);
 	    });

        //Médicos
        Route::group(['prefix' => 'medicos'], function () {
			Route::get('/', ['uses' => 'Qualitare\Hnsn\Controllers\Medicos@list']);
			Route::get('/{id}', ['uses' => 'Qualitare\Hnsn\Controllers\Medicos@show']);
			Route::post('/find', ['uses' => 'Qualitare\Hnsn\Controllers\Medicos@find']);
 	    });

        //Parceiros
        Route::group(['prefix' => 'parceiros'], function () {
			Route::get('/', ['uses' => 'Qualitare\Hnsn\Controllers\Parceiros@list']);
			Route::post('/find', ['uses' => 'Qualitare\Hnsn\Controllers\Parceiros@find']);
 	    });

        //Serviços
        Route::group(['prefix' => 'servicos'], function () {
			Route::get('/', ['uses' => 'Qualitare\Hnsn\Controllers\Servicos@list']);
			Route::post('/find', ['uses' => 'Qualitare\Hnsn\Controllers\Servicos@find']);
 	    });

        //Convenios
        Route::group(['prefix' => 'convenios'], function () {
            Route::get('/', ['uses' => 'Qualitare\Hnsn\Controllers\Convenios@list']);
        });

        //Videos
        Route::group(['prefix' => 'videos'], function () {
            Route::get('/', ['uses' => 'Qualitare\Hnsn\Controllers\Videos@list']);
        });


});
