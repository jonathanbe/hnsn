<?php namespace Qualitare\Hnsn\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareHnsnConvenios extends Migration
{
    public function up()
    {
        Schema::create('qualitare_hnsn_convenios', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
            $table->string('tipo', 30);
            $table->string('desc', 250);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_hnsn_convenios');
    }
}
