<?php namespace Qualitare\Hnsn\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Qualitare\Hnsn\Models\Servico;

class Servicos extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qualitare.Hnsn', 'main-menu-item3');
    }

    public function list(){
        return response(Servico::all(), 200);
    }

    public function find(Request $request){
        $q = $request->get('q');
        $query = Servico::where('name', 'LIKE', '%'.$q.'%')->get();

        return response($query, 200);
    }
}
