<?php namespace Qualitare\Hnsn\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Qualitare\Hnsn\Models\Newsletter;

class Newsletters extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qualitare.Hnsn', 'main-menu-item7');
    }

    public function insert(Request $request){
        $data = $request->all();
        try{
            $retorno = Newsletter::create($data);
            return response($retorno, 200);
        }catch( Exception $e ){
            return response($e->getMessage(), 400);
        }
    }
}
